function each(elements, callback) {
    let arr = []
    if (!Array.isArray(elements) || !callback) {
    } else {
        for (let index = 0; index < elements.length; index++) {
            arr.push(callback(elements[index], index, elements));
        }
        return arr
    }
}
module.exports = each