function filter(elements, callback) {
    let arr = []
    if (!Array.isArray(elements) || !callback) {
        return arr
    }
    for (let keys of elements) {
        if (callback(keys)) {
            arr.push(keys)
        }
    }
    return arr
}

module.exports = filter

