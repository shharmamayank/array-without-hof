function map(arrays, callback) {
    if (!Array.isArray(arrays) || !callback) {
        return undefined;
    }
    else {
        let number = [];
        for (let index = 0; index < arrays.length; index++) {
            number.push(callback(arrays[index], index, arrays))
        }
        return number
    }
}
module.exports = map